# Maintainer: Milan P. Stanić <mps@arvanta.net>

_flavor=edge
pkgname=linux-${_flavor}
# NOTE: this kernel is intended for testing
# please resist urge to upgrade it blindly
pkgver=5.17.1
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux latest stable kernel"
url="https://www.kernel.org"
depends="mkinitfs"
_depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any
	openssl1.1-compat-dev diffutils findutils xz"
options="!strip !check" # no tests
_config=${config:-config-edge.${CARCH}}
install=

subpackages="$pkgname-dev:_dev:$CBUILD_ARCH"
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz"
case $pkgver in
	*.*.0)	source="$source";;
	*.*.*)	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz" ;;
esac

	source="$source
	config-edge.aarch64
	config-edge.armv7
	config-edge.x86_64
	config-edge.riscv64
	config-edge4virt.aarch64
	config-edge4virt.armv7
	config-edge4virt.x86_64
	config-edge4virt.riscv64
	"

arch="armv7 aarch64 x86_64 riscv64"
license="GPL-2.0"

_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
riscv64) _carch="riscv" ;;
esac

prepare() {
	local _patch_failed=
	cd "$srcdir"/linux-$_kernver
	case $pkgver in
		*.*.0);;
		*)
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N ;;
	esac

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		local _builddir="$srcdir"/build-$i.$CARCH
		mkdir -p "$_builddir"
		echo "-$pkgrel-$i" > "$_builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$_builddir"/.config
		make -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$srcdir"/build-$i.$CARCH
		make ARCH="$_carch" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		riscv64) _install="install dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package edge "$pkgdir"
}

# subflavors install in $subpkgdir
edge4virt() {
	_package edge4virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine

	make -j1 -C "$srcdir"/linux-$_kernver O="$dir" ARCH="$_carch" \
		syncconfig prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the soruces
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

sha512sums="
89f0a7ca69d20a539d4b612a7028a30a5e98b402e4b6b88516f14237e5da4b626d7929eab8b40fccc90766e8f3bae87e9858a19077ffad20d8204acf18794f5b  linux-5.17.tar.xz
fb9a28ce7c68daeca82586e7631dc0be00a1a8393b4cef4c659fc1f9dac6e1f941bd60671c0350d1dea9cb035f56d627f64a394a9acb00b2b5dea8a1d44c065f  patch-5.17.1.xz
a68d3c1fddbce8481b846becc34f919872b6d6458e4f769262d340cc3a750d05ace53bbbf39a4ea09336b8c9c40511c297eef8b9474a46ebcba5a48c59f5282f  config-edge.aarch64
6fbf4fdd744a60fd9776de30c37a56a4e40b2aab8f375ec0ca0c310dcf2a031efca35a96d33255b10925b56e574c112c3f868b6ab7a5ab667d4f23c21ef3e73f  config-edge.armv7
87f4f3b103ad69f5d3d510e841bce307c36639a605f85df3e2956e2634b82c45baa47834d8fc8104807762d73706598eef3d960af2e3da4ca225339752f05972  config-edge.x86_64
da607715cda5ee0e2a17f8960482117c98a70d786c161f5866f3dd24b112167483aeb7adbb87789aa181c5ff59724b6086a38b2e624034f23eb01d6785130cdf  config-edge.riscv64
8aa5f87599891e9472d695a27ab279573e4db9a1606695086a052e6580eaf1068f0b79681be313ce6215370535e23751a2661795f8c4d5417ac36bf845f72c72  config-edge4virt.aarch64
ee2cb2f7b92e1b3a0cb1a7abbc7244a189504b6731da181bdf9484e5e3a114c2aeedb25fb8606b3fe6e761203195f34334b0a9d840387aab1f829fd423bc49f2  config-edge4virt.armv7
b1c04164fdbbe748c2fb402d00de89118ea139f6ab30708a33676bfe75f87d4fa16cbbae2944138e2d8b079432a888a9d7081e901cfc231aedb4613170c9cb4f  config-edge4virt.x86_64
c96c5ccd68853a3563fd430cce471d7574329226710d428e38858d538f20150e4210609a40d2c89b4ea0eba3427ffcb54a203303d9750dffbc74ea2a4be1735c  config-edge4virt.riscv64
"
