# Contributor: Jan Tatje <jan@jnt.io>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=qbittorrent
pkgver=4.4.2
pkgrel=1
pkgdesc="qBittorrent client"
url="https://www.qbittorrent.org/"
arch="all !s390x" # qt6-qttools
license="GPL-2.0-or-later"
options="!check" # qBittorrent has no tests
makedepends="
	boost-dev
	cmake
	libexecinfo-dev
	libtorrent-rasterbar-dev
	ninja
	qt6-qtbase-dev
	qt6-qtsvg-dev
	qt6-qttools-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-nox
	$pkgname-nox-openrc:nox_openrc
	$pkgname-nox-doc:nox_doc
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/qbittorrent/qBittorrent/archive/refs/tags/release-$pkgver.tar.gz
	qbittorrent-nox.initd
	qbittorrent-nox.confd
	link-execinfo.patch
	"
builddir="$srcdir/qBittorrent-release-$pkgver"

# secfixes:
#   4.1.6-r3:
#     - CVE-2019-13640

build() {
	cmake -B build-gui -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=ON \
		-DWEBUI=OFF \
		-DQT6=ON
	cmake --build build-gui

	cmake -B build-nox -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=ON \
		-DGUI=OFF \
		-DQT6=ON
	cmake --build build-nox
}

package() {
	depends="qt6-qtsvg" # needed for ui icons
	DESTDIR="$pkgdir" cmake --install build-gui
}

nox() {
	install="$pkgname-nox.pre-install"
	pkgdesc="$pkgdesc (webui)"

	install -Dm755 "$builddir"/build-nox/qbittorrent-nox \
		"$subpkgdir"/usr/bin/qbittorrent-nox
}

nox_doc() {
	install -Dm644 "$builddir"/doc/qbittorrent-nox.1 \
		"$subpkgdir"/usr/share/man/man1/qbittorrent-nox.1
	default_doc
}

nox_openrc() {
	depends="openrc"
	pkgdesc="$pkgdesc (webui) (OpenRC init scripts)"
	install_if="openrc ${subpkgname%-openrc}=$pkgver-r$pkgrel"

	install -Dm755 "$srcdir"/$pkgname-nox.initd \
		"$subpkgdir"/etc/init.d/$pkgname-nox
	install -Dm644 "$srcdir"/$pkgname-nox.confd \
		"$subpkgdir"/etc/conf.d/$pkgname-nox
}
sha512sums="
55656fb5fd282a3ed0e703b9b47ec9733a70cf6242cae956a5b2487ef2aeb88a04bf5d37c8fa88554edf95ab0821b76ebebb53e8fc43dc5889f8c730075d6e26  qbittorrent-4.4.2.tar.gz
22a0607efdc78b54f8532b905c091bc5f92fd62e601868b50a273c45ea8116f8002a81a623916c37fabd6d643ce396bb3602122d60064412c87da12753fe7600  qbittorrent-nox.initd
97f117fd28f53ce550e2eea2a81a723b4426dbb23db2086a5938f3e2b8b482846ecd87e0ec2c1ef8c268579bec96d04f771f3f3a151292668ca37e0948e27b86  qbittorrent-nox.confd
dfadae675d9a1b1262aa7d0f848e93dafeb20acff226aac5b88dd105481e9815319f2a71cdfbd46c2901cf4b283e0b14295e85e0c7a2ddfd21e43ea858c028ff  link-execinfo.patch
"
