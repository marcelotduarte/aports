# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swayr
pkgver=0.16.0
pkgrel=0
pkgdesc="A window switcher (and more) for Sway"
url="https://sr.ht/~tsdh/swayr/"
arch="aarch64 armhf armv7 ppc64le x86 x86_64"  # limited by rust/cargo
license="GPL-3.0-or-later"
makedepends="cargo"
source="$pkgname-$pkgver.tar.gz::https://git.sr.ht/~tsdh/swayr/archive/v$pkgver.tar.gz
	cargo-optimize.patch
	"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare
	cargo fetch --locked
}

build() {
	cargo build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	cargo install --locked --offline --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
101ae714b0b937ed4c0bfcb22e0ceceac0c08063ea8af8c62512051412ab5d6337dda9001895decc4a6e866c1b41d21ae946ad36d55bf93f663b770aecd3905a  swayr-0.16.0.tar.gz
85c7a2214f5d5ea5c601d38e5170fce3d0d49c78756208019904e9ea41134cc8b666365d436ef2993c0e3fcc37adb6dd9701842f8c6226240e03528cb883b3b5  cargo-optimize.patch
"
