# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.3.4
pkgrel=0
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="compose-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz"

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-"$pkgver"

export GOPATH=$srcdir/go
export GOCACHE=$srcdir/go-build
export GOTMPDIR=$srcdir

build() {
	PKG=github.com/docker/compose/v2
	local ldflags="-s -w -X '$PKG/internal.Version=v$pkgver'"
	go build -modcacherw -ldflags="$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list -modcacherw ./... | grep -Ev '/e2e(/|$)')"
	go test -modcacherw -short $pkgs
	./docker-compose compose version
}

package() {
	install -Dm755 docker-compose "$pkgdir$_plugin_installdir"/docker-compose
}

sha512sums="
812c62fd52ea7fd6396af762a01f8e32447e144117a79c763aecc6e90fdf7eca907a7598ec3e53d61194b07473bac67677dabe4cb797ce24b55377b2920eb6c5  compose-2.3.4.tar.gz
"
