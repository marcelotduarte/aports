# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=esbuild
pkgver=0.14.28
pkgrel=0
pkgdesc="Extremely fast JavaScript bundler and minifier"
url="https://esbuild.github.io/"
license="MIT"
arch="all !riscv64" # blocked by nodejs
makedepends="go nodejs"
source="https://github.com/evanw/esbuild/archive/v$pkgver/esbuild-$pkgver.tar.gz"

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"

build() {
	go build \
		-ldflags="-X main.version=$pkgver" \
		-v ./cmd/esbuild

	node scripts/esbuild.js npm/esbuild/package.json --version
	node scripts/esbuild.js ./esbuild --neutral

	# binary path override
	sed -i '1s#^#var ESBUILD_BINARY_PATH = "/usr/bin/esbuild";\n#' \
		npm/esbuild/lib/main.js
}

check() {
	go test ./...
}

package() {
	install -Dm755 esbuild "$pkgdir"/usr/bin/esbuild

	local destdir=/usr/lib/node_modules/esbuild

	install -d \
		"$pkgdir"/$destdir/bin \
		"$pkgdir"/$destdir/lib

	install -Dm644 -t "$pkgdir"/$destdir npm/esbuild/package.json
	install -Dm644 -t "$pkgdir"/$destdir/lib npm/esbuild/lib/*
	ln -s /usr/bin/esbuild "$pkgdir"/$destdir/bin/esbuild
}

sha512sums="
d8f8d549b93845f7e0430c04b2ac8571b0848444d16ea617b0d970c1b01c521b38505e5a2a252ce38f35a76b90a710565810735ff8761135d83deb027289450b  esbuild-0.14.28.tar.gz
"
